# Falconi Categories on Spring boot with FreeMarker, RESTful API documented using SWAGGER
#Author
@Sayatbek 
Dortmund, Germany
2018

#Clone the source code

Go to:
- https://bitbucket.org/sayatbek/flaconi-categories/src/master/

or run this bash command:
```
$ git clone https://sayatbek@bitbucket.org/sayatbek/flaconi-categories.git
```

## What you'll need
- JDK 1.8 or later
- Maven 3 or later

## Stack
- Spring Boot
- Java
- FreeMarker
- Swagger

##Build
`mvn clean verify`

## Run
`mvn spring-boot:run`

# Live instance for demonstration is available here:
- http://18.217.76.235:8585/

## API Documentation 
- http://127.0.0.1:8080/swagger-ui.html

## Functionality Examples
# 1 Clients can create a new category. RequestMethod.PUT
- http://localhost:8080/categories/insert
  {
  	"name" : "Base Category",
  	"slug" : "example_slug",
  	"isVisible" : "true",
  	"parentCategoryId" : "9567bb3e-b205-4388-99a6-c01f0300e3be"
  }

# Clients can retrieve a category by an id. RequestMethod.GET
- http://127.0.0.1:8080/categories/get/id/e3520f6b-0eee-43ce-b886-ed44c04d803c

# Clients can retrieve a list of categories by a slug. RequestMethod.GET
- http://127.0.0.1:8080/categories/get/slug/example_slug

# Clients can retrieve a tree of children under a specific category. RequestMethod.GET
- http://127.0.0.1:8080/categories/get/tree/e3520f6b-0eee-43ce-b886-ed44c04d803c

# Clients can update the visibility status of a specific category. RequestMethod.PUT
- http://127.0.0.1:8080/categories/update/visibility/e3520f6b-0eee-43ce886-ed44c04d803c/true

## Docker
# Image on docker cloud
- https://cloud.docker.com/repository/docker/sayatbek/flaconi-categories

# You can build docker image using this command
`docker build . -t flaconi-categories`

# Or pull an image from cloud
`docker pull sayatbek/flaconi-categoties`

# Run docker image on port 8585
`docker run -d --restart=always -p 8585:8080 sayatbek/flaconi-categories`

