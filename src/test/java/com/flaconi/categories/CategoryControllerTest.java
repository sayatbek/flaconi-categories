package com.flaconi.categories;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.flaconi.categories.controller.CategoryController;
import com.flaconi.categories.crud.CategoriesRepository;
import com.flaconi.categories.model.Category;
import com.flaconi.categories.service.CategoryService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryControllerTest {

  MockMvc mockMvc;

  @Autowired
  private WebApplicationContext wac;

  @Autowired
  CategoryController categoryController;

  @Autowired
  CategoryService categoryService;

  @Autowired
  CategoriesRepository categoriesRepository;

  @Before
  public void setup() {
    this.mockMvc = standaloneSetup(this.categoryController).build();
  }

  @Test
  public void testInsertCategory() throws Exception {
    String request = "{\"name\" : \"Test MVC\",\"slug\" : \"mvc_test\",\"isVisible\" : \"true\"}";

    mockMvc.perform(put("/categories/insert")
        .contentType(MediaType.APPLICATION_JSON)
        .content(request))
        .andExpect(status().isCreated());

    request = "{\"name\" : \"Test MVC\",\"slug\" : \"mvc_test\",\"isVisible\" : \"true\",\"parentCategoryId\" : \"bad_request\"}";
    mockMvc.perform(put("/categories/insert")
        .contentType(MediaType.APPLICATION_JSON)
        .content(request))
        .andExpect(status().isBadRequest());

    categoriesRepository.delete(categoryService.getCategoriesBySlug("mvc_test"));
  }

  @Test
  public void getCategoryById() throws Exception {
    mockMvc.perform(get("/categories/get/id/wrong_id")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());

    Category rootCategory = new Category();
    rootCategory.setSlug("id_test_slug");
    rootCategory.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(rootCategory);

    Category item = categoryService.getCategoriesBySlug("id_test_slug").get(0);
    MvcResult result = mockMvc.perform(get("/categories/get/id/" + item.getId())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();

    String content = result.getResponse().getContentAsString();
    Category resultCategory = new Gson().fromJson(content, Category.class);
    categoriesRepository.delete(item);
    assertEquals(item.getId(), resultCategory.getId());
  }

  @Test
  public void getCategoryBySlug() throws Exception {
    Category rootCategory = new Category();
    rootCategory.setSlug("slug_test_slug");
    rootCategory.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(rootCategory);

    Category item = categoryService.getCategoriesBySlug("slug_test_slug").get(0);
    MvcResult result = mockMvc.perform(get("/categories/get/slug/slug_test_slug")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();

    String content = result.getResponse().getContentAsString();
    if (content.charAt(0) == '[') {
      content = content.substring(1, content.length() - 1);
    }
    Category resultCategory = new Gson().fromJson(content, Category.class);
    categoriesRepository.delete(item);
    assertEquals(item.getSlug(), resultCategory.getSlug());
  }

  @Test
  public void testUpdateVisibility() throws Exception {
    Category rootCategory = new Category();
    rootCategory.setSlug("visibility_test_slug");
    rootCategory.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(rootCategory);

    Category item = categoryService.getCategoriesBySlug("visibility_test_slug").get(0);
    mockMvc.perform(put("/categories/update/visibility/" + item.getId() + "/" + false)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();

    Category updatedCategory = categoriesRepository.findById(item.getId());
    categoriesRepository.delete(updatedCategory);
    assertEquals(Boolean.FALSE, updatedCategory.getIsVisible());
  }

  @Test
  public void testGetTreeOfChildren() throws Exception {
    Category rootCategory = new Category();
    rootCategory.setName("Root");
    rootCategory.setSlug("mvc_root_test_slug");
    rootCategory.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(rootCategory);

    Category rootItem = categoryService.getCategoriesBySlug("mvc_root_test_slug").get(0);
    Category child = new Category();
    child.setName("Child");
    child.setSlug("mvc_child_test_slug");
    child.setParentCategory(rootItem);
    child.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(child);

    MvcResult result = mockMvc.perform(get("/categories/get/tree/" + rootItem.getId())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();
    String content = result.getResponse().getContentAsString();

    categoriesRepository.delete(categoryService.getCategoriesBySlug("mvc_child_test_slug"));
    categoriesRepository.delete(categoryService.getCategoriesBySlug("mvc_root_test_slug"));

    String expected = "Root\n\tChild\n\t";
    assertEquals(expected, content);
  }
}