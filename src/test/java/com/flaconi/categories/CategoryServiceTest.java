package com.flaconi.categories;

import static org.junit.Assert.assertEquals;

import com.flaconi.categories.crud.CategoriesRepository;
import com.flaconi.categories.model.Category;
import com.flaconi.categories.service.CategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceTest {

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private CategoriesRepository categoriesRepository;

  @Test
  public void testInsertCategory() {
    String request = "{\n"
        + "\t\"name\" : \"Test Node\",\n"
        + "\t\"slug\" : \"test_slug\",\n"
        + "\t\"isVisible\" : \"true\"\n"
        + "}";

    categoryService.insertCategory(request);

    Category item = categoryService.getCategoriesBySlug("test_slug").get(0);
    categoriesRepository.delete(item);
    assertEquals("test_slug", item.getSlug());
  }

  @Test
  public void testGetCategoryBySlug() {
    Category temp = new Category();
    temp.setSlug("test_slug");
    temp.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(temp);

    Category item = categoryService.getCategoriesBySlug("test_slug").get(0);
    categoriesRepository.delete(item);
    assertEquals("test_slug", item.getSlug());
  }

  @Test
  public void testUpdateVisibility() {
    Category temp = new Category();
    temp.setSlug("test_slug");
    temp.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(temp);

    Category item = categoryService.getCategoriesBySlug("test_slug").get(0);
    categoryService.updateVisibility(item.getId(), Boolean.FALSE);

    Category updatedItem = categoriesRepository.findById(item.getId());
    categoriesRepository.delete(updatedItem);

    assertEquals(Boolean.FALSE, updatedItem.getIsVisible());
  }

  @Test
  public void testGetTreeOfChildren() {
    Category rootCategory = new Category();
    rootCategory.setName("Root");
    rootCategory.setSlug("root_test_slug");
    rootCategory.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(rootCategory);

    Category rootItem = categoryService.getCategoriesBySlug("root_test_slug").get(0);
    Category child = new Category();
    child.setName("Child");
    child.setSlug("child_test_slug");
    child.setParentCategory(rootItem);
    child.setIsVisible(Boolean.TRUE);
    categoriesRepository.save(child);

    String result = categoryService.getTreeOfChildren(rootItem.getId());

    categoriesRepository.delete(categoryService.getCategoriesBySlug("child_test_slug"));
    categoriesRepository.delete(categoryService.getCategoriesBySlug("root_test_slug"));

    String expected = "Root\n\tChild\n\t";
    assertEquals(expected, result);
  }
}
