package com.flaconi.categories.service.impl;

import com.flaconi.categories.crud.CategoriesRepository;
import com.flaconi.categories.model.Category;
import com.flaconi.categories.model.TreeNode;
import com.flaconi.categories.service.CategoryService;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import java.util.List;
import java.util.UUID;
import javax.validation.ConstraintViolationException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

  private static final Logger logger = LogManager.getLogger(CategoryServiceImpl.class);

  @Autowired
  private CategoriesRepository categoriesRepository;

  @Override
  public boolean insertCategory(String request) {
    try {
      Category category = new Gson().fromJson(request, Category.class);
      if (category.getParentCategoryId() != null) {
        Category parentCategory = categoriesRepository.findById(category.getParentCategoryId());
        if (parentCategory == null) {
          return Boolean.FALSE;
        }
        category.setParentCategory(parentCategory);
      }
      categoriesRepository.save(category);
      return Boolean.TRUE;
    } catch (JsonParseException e) {
      logger.error(e.getLocalizedMessage());
    } catch (ConstraintViolationException e) {
      logger.error(e.getLocalizedMessage());
    } catch (IllegalArgumentException e) {
      logger.error(e.getLocalizedMessage());
    }
    return Boolean.FALSE;
  }

  @Override
  public Category getCategoryById(UUID id) {
    return categoriesRepository.findById(id);
  }

  @Override
  public List<Category> getCategoriesBySlug(String slug) {
    return categoriesRepository.findAllBySlugAndIsVisible(slug, Boolean.TRUE);
  }

  @Override
  public Category updateVisibility(UUID id, Boolean visibility) {
    Category category = categoriesRepository.findById(id);
    if (category != null) {
      category.setIsVisible(visibility);
      categoriesRepository.save(category);
    }
    return category;
  }

  @Override
  public String getTreeOfChildren(UUID id) {
    Category category = categoriesRepository.findById(id);
    TreeNode root = new TreeNode(category, 0);

    addChildren(root, id);

    return root.printTree(root);
  }

  public void addChildren(TreeNode root, UUID id) {
    List<Category> children = categoriesRepository.findAllByParentCategoryId(id);
    for (Category child : children) {
      TreeNode childNode = root.addChild(child);
      addChildren(childNode, child.getId());
    }
  }
}
