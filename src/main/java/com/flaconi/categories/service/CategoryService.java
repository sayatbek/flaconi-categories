package com.flaconi.categories.service;

import com.flaconi.categories.model.Category;
import java.util.List;
import java.util.UUID;

public interface CategoryService {

  boolean insertCategory(String request);

  Category getCategoryById(UUID id);

  List<Category> getCategoriesBySlug(String slug);

  Category updateVisibility(UUID id, Boolean visibility);

  String getTreeOfChildren(UUID id);
}