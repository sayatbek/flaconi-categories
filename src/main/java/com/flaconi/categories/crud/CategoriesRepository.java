package com.flaconi.categories.crud;

import com.flaconi.categories.model.Category;
import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesRepository extends CrudRepository<Category, UUID> {

  List<Category> getAllByIsVisible(Boolean isVisible);

  Category findById(UUID id);

  List<Category> findAllBySlugAndIsVisible(String slug, Boolean isVisible);

  List<Category> findAllByParentCategoryId(UUID id);
}
