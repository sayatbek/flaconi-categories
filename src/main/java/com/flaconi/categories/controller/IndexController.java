package com.flaconi.categories.controller;

import com.flaconi.categories.crud.CategoriesRepository;
import com.flaconi.categories.model.Category;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

  @Autowired
  private CategoriesRepository categoriesRepository;

  @GetMapping("/")
  public String index(Model model) {
    List<Category> categoryList = categoriesRepository.getAllByIsVisible(Boolean.TRUE);
    model.addAttribute("categoryList", categoryList);
    return "index";
  }
}