package com.flaconi.categories.controller;

import com.flaconi.categories.model.Category;
import com.flaconi.categories.service.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/categories")
public class CategoryController {

  @Autowired
  private CategoryService categoryService;

  @RequestMapping(value = "/insert", method = RequestMethod.PUT, produces = "application/json")
  @ApiOperation(value = "Create new category item and store it in DB")
  public ResponseEntity insertCategory(
      @ApiParam(value = "Category object in Json format", required = true)
      @RequestBody String request
  ) {
    return categoryService.insertCategory(request) ? new ResponseEntity(HttpStatus.CREATED)
        : new ResponseEntity(HttpStatus.BAD_REQUEST);
  }

  @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET, produces = "application/json")
  @ApiOperation(value = "Get category item by it's 'id' number")
  public ResponseEntity<Category> getCategoryById(
      @ApiParam(name = "id", value = "id number of the category being retrieved", required = true)
      @PathVariable UUID id
  ) {
    return new ResponseEntity(categoryService.getCategoryById(id), HttpStatus.OK);
  }

  @RequestMapping(value = "/get/slug/{slug}", method = RequestMethod.GET, produces = "application/json")
  @ApiOperation(value = "Get the list of visible categories by their 'slug' name")
  public ResponseEntity<List<Category>> getCategoriesBySlug(
      @ApiParam(name = "slug", value = "slug name of the categories being retrieved", required = true)
      @PathVariable String slug
  ) {
    return new ResponseEntity(categoryService.getCategoriesBySlug(slug), HttpStatus.OK);
  }

  @RequestMapping(value = "/get/tree/{id}", method = RequestMethod.GET, produces = "application/json")
  @ApiOperation(value = "Get the Tree of children categories")
  public ResponseEntity<String> getTreeOfChildren(
      @ApiParam(name = "id", value = "id number of the root category", required = true)
      @PathVariable UUID id) {
    return new ResponseEntity(categoryService.getTreeOfChildren(id), HttpStatus.OK);
  }

  @RequestMapping(value = "/update/visibility/{id}/{visibility}", method = RequestMethod.PUT, produces = "application/json")
  @ApiOperation(value = "Change the visibility of a given category")
  public ResponseEntity updateVisibility(
      @ApiParam(name = "id", value = "id number of the category veing changed", required = true)
      @PathVariable UUID id,
      @ApiParam(name = "visibility", value = "true / false", required = true)
      @PathVariable Boolean visibility) {
    Category editedCategory = categoryService.updateVisibility(id, visibility);
    return editedCategory != null ? new ResponseEntity(editedCategory, HttpStatus.OK)
        : new ResponseEntity(HttpStatus.BAD_REQUEST);
  }
}
