package com.flaconi.categories.model;

import java.util.ArrayList;

public class TreeNode<T> {

  private T value;
  private ArrayList<TreeNode<T>> children;
  private int childCount = 0;
  private int level;

  public TreeNode(T value, int level) {
    this.children = new ArrayList<>();
    this.value = value;
    this.level = level;
  }

  public TreeNode addChild(T value) {
    TreeNode newChild = new TreeNode(value, this.level + 1);
    children.add(newChild);
    childCount++;
    return newChild;
  }

  private String traverse(TreeNode obj) {
    String result = "\t";
    if (obj != null) {
      for (int i = 0; i < obj.childCount; i++) {
        for (int k = 0; k < obj.level; k++) {
          result += "\t";
        }
        result += ((TreeNode) obj.children.get(i)).value + "\n" + traverse(
            (TreeNode) obj.children.get(i));
      }
    }
    return result;
  }

  public String printTree(TreeNode obj) {
    return obj.value + "\n" + traverse(obj);
  }
}