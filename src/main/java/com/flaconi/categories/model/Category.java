package com.flaconi.categories.model;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "categories", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
public class Category implements Serializable {

  @Id
  @GeneratedValue
  private UUID id;

  private String name;
  private String slug;

  @JoinColumn(name = "parentCategoryId")
  @ManyToOne(targetEntity = Category.class, fetch = FetchType.EAGER)
  private Category parentCategory;

  @Column(name = "parentCategoryId", insertable = false, updatable = false)
  private UUID parentCategoryId;

  private Boolean isVisible;

  @Override
  public String toString() {
    //return new Gson().toJson(this);
    return this.name;
  }
}
