<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Falconi Categories on Spring boot with FreeMarker, RESTful API documented using SWAGGER</title>
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
    <div align="center">
        <a href="/swagger-ui.html">
            <h2 class="hello-title">Api Documentation Swager</h2>
        </a>
    </div>

    <select id="chosenCategory">
    <#list categoryList as item>
        <option value="${item.id}">
          ${item.name}
        </option>
    </#list>
    </select>
    <input type="button" value="GetChildTree" onclick="printChildren()">

    <script src="/js/main.js"></script>
</body>
</html>